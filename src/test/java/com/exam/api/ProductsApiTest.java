package com.exam.api;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import javax.ws.rs.core.Response;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.exam.domain.Product;
import com.jayway.restassured.http.ContentType;
//

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductsApiTest extends FunctionalTest {

	@Override
	protected String configureSubDomain() {
		return "products";
	}

	@Test
	public void testGetProductExcludingRelationships() {
		given().when().get("nochild/{id}", 1).then().statusCode(Response.Status.OK.getStatusCode());
	}

	@Test
	public void testGetAllProducts() {
		 given().when().get().then().statusCode(Response.Status.OK.getStatusCode());
	}

	@Test
	public void testGetProductWithFetch() {
		 given().when().get("{id}", 1).then().body("name",
		 equalTo("Computer")).statusCode(Response.Status.OK.getStatusCode());
	}

	@Test
	public void testSave() {

		Product product = new Product.ProductBuilder(null, "Product 5").build();
		//
		given().contentType(ContentType.JSON).body(product).when().post().then().body("name", equalTo("Product 5"))
				.statusCode(Response.Status.OK.getStatusCode());
	}

	@Test
	@Ignore
	public void testUpdate() {
		Product product = new Product.ProductBuilder(1, "Black Computer").build();

		given().contentType(ContentType.JSON).body(product).when().put("{id}", product.getId().toString()).then()
				.body("name", equalTo("Black Computer")).statusCode(Response.Status.OK.getStatusCode());
	}

	@Test
	public void testDelete() {
		given().when().delete("{id}", 2).then().statusCode(Response.Status.OK.getStatusCode());
	}

	@Ignore
	@Test
	public void testDeleteFail() {
		given().when().delete("{id}", 1).then().statusCode(Response.Status.FORBIDDEN.getStatusCode());
	}

	//

}
////